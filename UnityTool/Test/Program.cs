﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

using Common;
using Common.Maths;

using UnityTool;

using MyMesh = Common.Maths.Mesh;
using Mesh = UnityTool.Mesh;

namespace Test
{
    internal class Program
    {
        static string path = @"C:\Users\johnw\Projects\Empyrion\bundles\";

        static void TestB_1(string bundleFilename)
        {
            using (UnityFileReader reader = new UnityFileReader(path + bundleFilename))
            {
                if (!reader.LoadFile(out BundleFile bundle)) return;

                for (int i = 0; i < bundle.FilesCount; i++)
                {
                    var res = bundle.ExportFileStream(reader, i, path);
                    Debugg.Info($"Exported resource : {res}");
                }
            }
            Console.WriteLine("TestB_1 done");
        }

        static void TestA_1(string filename)
        {
            List<EmpyrionModel> Models = new List<EmpyrionModel>();
            List<GameObject> MainObjects = new List<GameObject>();
            int count = 0;

            using (UnityFileReader unityreader = new UnityFileReader(path + filename))
            {
                if (!unityreader.LoadFile(out AssetFile asset)) return;

                for (int i = 0; i < asset.ObjectCount; i++)
                {
                    var info = asset.ObjectInfos[i];

                    //i take only GameObjects
                    if (info.classID == ClassIDType.GameObject)
                    {
                        var gameobj = (GameObject)asset.GetObjectByIndex(unityreader, i);
                        {
                            //all gameobject must be linked before use transform
                            if (gameobj.Transform == null)
                                gameobj.LinkTransforms(unityreader);

                            if (gameobj.Transform != null)
                            {
                                if (gameobj.Transform.Father.IsNull)
                                {
                                    Debugg.Info($"{gameobj.Name}");
                                    MainObjects.Add(gameobj);
                                    //goto complete;
                                }

                                //if (gameobj.Transform.Father.TryGet(unityreader, out var Father))
                                    //if (m_Father.m_GameObject.TryGet(out var m_ParentGameObject)) { }
                            }
                        }
                    }
                }

            complete:
                Debugg.Warning($"Find {MainObjects.Count} main gameobjects");


                foreach (var gameobj in MainObjects)
                {
                    if (gameobj.Name == "FuelTankSVSmallPrefab")
                    {

                        EmpyrionModel mesh = EmpyrionModel.FromAsset(gameobj, unityreader);
                        Models.Add(mesh);

                        MyMesh bigone = MyMesh.MergeAllGeometries(mesh.Tree.GetElementCollection<MyMesh>());
                        var file = bigone.ConvertToWavefront();
                        file.Save(@"C:\Users\johnw\Desktop\" + gameobj.Name + "_exported");
                    }
                }
            }

            path += "_" + Path.GetFileNameWithoutExtension(filename) + "\\";
            if (!Directory.Exists(path)) 
                Directory.CreateDirectory(path);

            //default shape material
            /*
            WaveFileMat shapeMat = new WaveFileMat();
            WaveMaterial.Create(shapeMat, "North", Vector4b.Red);
            WaveMaterial.Create(shapeMat, "South", Vector4b.Green);
            WaveMaterial.Create(shapeMat, "West", Vector4b.Blue);
            WaveMaterial.Create(shapeMat, "Est", Vector4b.Mangenta);
            WaveMaterial.Create(shapeMat, "Top", Vector4b.Yellow);
            WaveMaterial.Create(shapeMat, "Bottom", Vector4b.Cyano);
            shapeMat.Save(path + "ShapeDefault.mat");
            */

            XmlWriterSettings setting = new XmlWriterSettings() { Indent = true, ConformanceLevel = ConformanceLevel.Fragment };

            foreach (var model in Models)
            {
                using (var writer = XmlWriter.Create(path + model.Name + ".xml", setting))
                {
                    model.Write(writer);
                }
                using (var file = File.Create(path + model.Name + ".tree"))
                using (var writer = new BinaryWriter(file))
                {
                    model.Write(writer);
                }


                /*
                var modelmesh = Common.Maths2.Mesh.MergeAllGeometries(model.Geometries);
                if (modelmesh != null)
                {
                    modelmesh.Name = model.Name;
                    var wavefront = modelmesh.ConvertToWavefront();

                    wavefront.MaterialFilename = "ShapeDefault.mat";
                    wavefront.Save(path + modelmesh.Name);
                }*/
            }

        }
        static void TestA_2()
        {
            //string filename = "CAB-209ca4128cc225d140b0877f1e548b48";
            string filename = "CAB-13403b2e592bfd37bb36e3233e9dbc18";

            List<GameObject> MainObjects = new List<GameObject>();
            int count = 0;

            using (UnityFileReader unityreader = new UnityFileReader(path + filename))
            {
                if (!unityreader.LoadFile(out AssetFile asset)) return;

                for (int i = 0; i < asset.ObjectCount; i++)
                {
                    var info = asset.ObjectInfos[i];

                    //i take only GameObjects
                    if (info.classID == ClassIDType.Mesh)
                    {
                        var mesh = (Mesh)asset.GetObjectByIndex(unityreader, i);
                        
                    }
                }
            }

        }


        static void TestC(string filename = @"C:\Users\johnw\Desktop\RampD.treemesh")
        {
            using (var file = File.OpenRead(filename))
            using (var reader = new BinaryReader(file))
            {
                EmpyrionModel model = new EmpyrionModel(reader);
            }
        }

        static void Main(string[] args)
        {
            //CAB-c1a49265055a46ebce7ecb04462dfa0c
            //CAB-209ca4128cc225d140b0877f1e548b48
            TestA_1("CAB-209ca4128cc225d140b0877f1e548b48");

            //TestB_1("shapes");
            //TestA_1();
            Debugg.Info("completed");
            Console.ReadKey();
        }
    }
}
