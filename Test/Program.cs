﻿using System;
using System.Collections.Generic;

using AssetStudio;
using Blue3DPrinter;

using FbxTool;

using Common.IO.Wavefront;
using Common.Tools;

namespace Test
{
    class Program
    {
        static string GameDirectory = @"C:\Program Files (x86)\Steam\steamapps\common\Empyrion - Galactic Survival";

        static string[] BundlesPath = new string[]
        {
            @"\Content\Bundles\shapes",
            @"\Content\Bundles\models",
            @"\Content\Bundles\models2",
        };

        static void Main(string[] args)
        {
            TestC();


            Console.WriteLine("Press any key to stop...");
            Console.ReadKey();
        }

        static void TestB()
        {
            //test reforged eden blockid over 2048
            BlocksConfig config = BlocksConfig.LoadConfig(@"C:\Program Files (x86)\Steam\steamapps\workshop\content\383120\2174854289\Content\Configuration\BlocksConfig.ecf");

            config.TryGetFromName("ThrusterSVRoundSlantT2", out BlockDescription desrc, out int childindex);

            desrc.ToString();
            //BlocksConfig config = BlocksConfig.LoadConfig("Test.ecf");
        }

        static void TestA()
        {
            /*
            BlocksConfig config = BlocksConfig.LoadConfig();
            FileStorageManager mystorage = FileStorageManager.LoadOrCreate(@"D:\Users\john\Progetti\Empyrion\Blue3DPrint\Models\modelStorage");

            BlockDescription description = config.GetDescription(456);
            string shapename = description.GetFilenameAsset(0);

            mystorage.OpenAndLoad();

            var file = mystorage.GetFile(shapename);
            
            if (file is SceneFile scene && scene.TotalObjects > 0)
            {
                BlockModel model = new BlockModel(scene, description, shapename, true);

                var vertexMap = new BitArray1(model.Mesh.VerticesCount, true);
                var triangleMap = new BitArray1(model.Mesh.TrianglesCount, false);
                //triangleMap[4] = true;

                //Mesh mesh = model.Mesh.RemapMesh(vertexMap, triangleMap);
                Mesh mesh = model.Mesh;

                WaveFileObj wave = mesh.ConvertToWavefront(true);
                
                wave.Save(@"D:\Users\john\Progetti\Empyrion\Blue3DPrint\Models\" + scene.Name + "_testA.obj");
            }

            mystorage.Close();
            */
        }

        static void TestH()
        {
            /*
            BlocksConfig config = BlocksConfig.LoadConfig();
            FileStorageManager mystorage = FileStorageManager.LoadOrCreate(@"D:\Users\john\Progetti\Empyrion\Blue3DPrint\Models\modelStorage");

            Blueprint blueprint = Blueprint.Open(@"D:\Users\john\Progetti\Empyrion\Blue3DPrint\Sample1\BPR_Hide_Posfix.epb");

            if (blueprint.Blocks.LoadDescrAndModels(config, mystorage))
            {
                SceneGenerator.GenerateWaveFront(blueprint, blueprint.Filename, HideLevel.Complete, true);
            }
            */
        }
        static void TestC()
        {
            /*
            BundleUtilities assetool = new BundleUtilities();
            List<GameObject> objects = assetool.GetMainObjects(@"C:\Program Files (x86)\Steam\steamapps\common\Empyrion - Galactic Survival\Content\Bundles\models");
            foreach(var obj in objects)
            {
                //if (obj.m_Name == "CoreBlockPlayerPrefab")
                if (obj.m_Name == "ThrusterSSDirectionalPrefab")
                {
                    SceneFile scene = BundleUtilities.ConvertAssetToScene(obj);
                    BlockModel model = new BlockModel(scene, null, scene.Name, true);

                    var file = model.Mesh.ConvertToWavefront();
                    file.Save(@"D:\Users\john\Progetti\Empyrion\Blue3DPrint\Models\ThrusterSSDirectionalPrefab_bundle.obj");


                    return;
                }
            }
            */

        }
        static void TestFBX()
        {
            /*
            string filename = @"D:\Users\john\Progetti\Empyrion\Blue3DPrint\ModelsFbx\Ramp3x5x3Prefab.fbx";

            FbxImporter.Validate();

            SceneFile file = FbxImporter.Import(filename, FbxImporter.skipEmpyrionFbxNodeName);

            BlockModel model = new BlockModel(file, null, file.Name);

            Mesh mesh = model.Mesh;

            WaveFileObj obj = mesh.ConvertToWavefront();

            obj.Save(@"D:\Users\john\Progetti\Empyrion\" + file.Name + "_fromfbx.obj");
            */
        }
    }
}
