# EMPYRION'S BLUEPRINT CONVERTER(WIP)

## Visual Studio 2019

- The purpose of this tool is to convert the Empyrion's blueprint files to 3d files, then create models with your 3d printer.
No animations, textures, etc ... will be imported, only the "brutal-raw" geometries.

- The library to read asset is imported from https://github.com/Perfare/AssetStudio

- Requires my libraries;
	https://bitbucket.org/johnwhile/fbxwrapper : a custom fbx wrapper to work with last fbx files
	https://bitbucket.org/johnwhile/commonlib  : my collection of maths and functions also for other projects

- I'm not a programmer but for some reason the code work, if you found compiling issue, ask me to johnwhilemail@gmail.com

